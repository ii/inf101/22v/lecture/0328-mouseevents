package inf101v22.gfx;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class App {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("INF101");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel mainPanel = new MainPanel();
        frame.setContentPane(mainPanel);

        frame.pack();
        frame.setVisible(true);
    }
    
}
