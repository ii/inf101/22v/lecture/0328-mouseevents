package inf101v22.gfx;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class MainPanel extends JPanel {

    JPanel sub1 = new SubPanel(Color.BLUE);
    JPanel sub2 = new SubPanel(Color.YELLOW);

    public MainPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createEmptyBorder(10, 20, 30, 40));
        this.add(sub1);
        this.add(Box.createRigidArea(new Dimension(0, 100)));
        this.add(sub2);
    }

}
