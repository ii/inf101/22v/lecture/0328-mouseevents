package inf101v22.gfx;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

public class SubPanel extends JPanel implements MouseListener, MouseMotionListener {

    private Color bgColor;
    private boolean mouseIsHere = false;
    private int x;
    private int y;
    private int radius = 10;

    public SubPanel(Color bgColor) {
        this.bgColor = bgColor;
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(this.mouseIsHere ? bgColor.darker() : bgColor);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        g.setColor(Color.WHITE);
        g.fillOval(x - radius, y - radius, radius * 2, radius * 2);
    }

    @Override
    public Dimension preferredSize() {
        return new Dimension(300, 100);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // this.x = e.getX();
        // this.y = e.getY();
        // this.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // ignored
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // ignored
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.mouseIsHere = true;
        this.repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.mouseIsHere = false;
        this.repaint();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // ignored
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        this.x = e.getX();
        this.y = e.getY();
        this.repaint();
    }

}
